/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import com.wpsj.entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Puarinnnn
 */
public class ProductDataAcess {
    private PreparedStatement searchStatement;
    private PreparedStatement getSearchStatement() throws ClassNotFoundException,
            SQLException{
    if(searchStatement == null){
        Connection connection = new DBConnection().getConnection();
        searchStatement = connection.prepareStatement("SELECT pro_id, pro_name, pro_desc FROM ProductStore WHERE pro_name like ?");
        
    }
    return searchStatement;
    }
         
    public List<Product> getProductsByName(String name){
    try{
        PreparedStatement statement = getSearchStatement();
        statement.setString(1, "%"+name+"%");
        ResultSet rs = statement.executeQuery();
        List<Product> products = new LinkedList<Product>();
        while(rs.next()){
            products.add(new Product(rs.getInt("pro_id"),
                     rs.getString("pro_name"),rs.getString("pro_desc")));
            
        }
        return products;
    }catch(Exception e){
        e.printStackTrace();
        return null;
    }
    }


    public void editProduct(Product p) throws ClassNotFoundException, SQLException {
        Connection conn = new DBConnection().getConnection();
        PreparedStatement sta = conn.prepareStatement("UPDATE ProductStore set pro_name=?,pro_desc=? WHERE pro_id=?");
        sta.setString(1, p.getName());
        sta.setInt(2, p.getId());
        sta.setString(3, p.getDesc());
        sta.executeUpdate();
        sta.close();
    }
    public void deleteProduct(String name) throws ClassNotFoundException, SQLException{
    Connection conn = new DBConnection().getConnection();
    PreparedStatement sta = conn.prepareStatement("DELETE FROM ProductStore WHERE pro_name"+name);
    sta.executeUpdate();
    }

}
